package run;

import cars.*;
import usermenu.Menu;
import java.util.*;

/**
 *
 * @author Borimir Georgiev
 */
public abstract class App
{
    public static void run()
    {
        // create cars
        Car bmw = new Bmw("M5", "petrol", 3.5);
        Car audi = new Audi("A5", "diesel", 2.0);
        Car mercedes = new Mercedes("C", "petrol", 5.5);
        Car infinity = new Infinity("FX45", "petrol", 4.5);

        // add them to list
        ArrayList<Car> cars = new ArrayList<>();
        cars.add(bmw);
        cars.add(audi);
        cars.add(mercedes);
        cars.add(infinity);

        // constructs a menu and returns the user choice to be worked with.
        Menu menu = new Menu(cars);
        menu.toggleOptions();
        int choice = menu.read();

        System.out.println(cars.get(choice));
    }
}
