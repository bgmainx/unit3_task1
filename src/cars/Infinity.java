package cars;

public class Infinity extends Car
{
    public Infinity(String model, String fuel, double engineCapacity)
    {
        super(model, fuel, engineCapacity);
    }

    @Override
    protected double calculatePrice()
    {
        return super.getEngineCapacity() * 21200;
    }
}
