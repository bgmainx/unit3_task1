package cars;

public class Mercedes extends Car
{
    public Mercedes(String model, String fuel, double engineCapacity)
    {
        super(model, fuel, engineCapacity);
    }

    @Override
    protected double calculatePrice()
    {
        return super.getEngineCapacity() * 31000;
    }
}
