package cars;

public class Bmw extends Car
{
    public Bmw(String model, String fuel, double engineCapacity)
    {
        super(model, fuel, engineCapacity);
    }

    @Override
    protected double calculatePrice()
    {
        return super.getEngineCapacity() * 27400;
    }


}
