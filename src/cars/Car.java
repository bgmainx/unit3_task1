package cars;
/**
 * Simple Car class which provides basic behaviour and functionality to its successors.
 *
 * @author Borimir Georgiev
 */

public abstract class Car
{
   private String model;
   private String fuel;
   private double engineCapacity;

   Car(String model, String fuel, double engineCapacity)
   {
      this.model = model;
      this.fuel = fuel;
      this.engineCapacity = engineCapacity;
   }

   protected double getEngineCapacity()
   {
      return this.engineCapacity;
   }



   protected abstract double calculatePrice();

   @Override
   public String toString()
   {
      return "Brand: " + this.getClass().getSimpleName() + "\n" +
              "Model: " + model +  "\n" +
              "Fuel: " + fuel + "\n" +
              "Engine capacity: " + engineCapacity + "\n" +
              "Price: " + Math.round(this.calculatePrice()) + "\n";
   }
}
