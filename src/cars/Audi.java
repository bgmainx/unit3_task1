package cars;

public class Audi extends Car
{
    public Audi(String model, String fuel, double engineCapacity)
    {
        super(model, fuel, engineCapacity);
    }

    @Override
    protected double calculatePrice()
    {
        return super.getEngineCapacity() * 23800;
    }


}
