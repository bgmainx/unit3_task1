package usermenu;

import java.util.List;
import java.util.Scanner;

/**
 * Simple menu displaying different options to the user. Reading the input, validating and returning the correct value.
 *
 * Found redundant to write a method which accepts user choice as param and takes action accordingly,
 * because it prevents the client to work in different ways with the "choice".
 *
 * @author Borimir Georgiev
 */

public class Menu
{
    private Scanner sc;
    private List list;

    public Menu(List list)
    {
        this.sc = new Scanner(System.in);
        this.list = list;
    }

    /**
     * Constructs a list of options and prints them.
     */
    public void toggleOptions()
    {
        int count = 1;

        for (Object o : list)
        {
            System.out.println(count + ". " + o.getClass().getSimpleName());
            count++;
        }
    }

    /**
     * Reads and validates input. Returns the input choice to be used as per client choice.
     *
     * @return users choice as an int.
     */

    public int read()
    {
        int userChoice = 0;

        // Checks if it's a number.
        try
        {
            userChoice = Integer.parseInt(sc.nextLine());
        } catch (NumberFormatException e)
        {
            System.out.println("Invalid choice! Input must be a number.");
            toggleOptions();
            read();
        }

        // Checks if the list of options contains a number same as the users num.
        if (userChoice <= list.size())
        {
            return userChoice;

        } else
        {
            System.out.println("There isn't a choice with such number.");
            System.out.println("Please input a number between 1 and " + list.size());
            toggleOptions();
            return read();
        }
    }
}
